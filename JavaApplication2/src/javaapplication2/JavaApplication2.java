/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.util.Scanner;

/**
 *
 * @author DAM125
 */
public class JavaApplication2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        boolean salir = false;
        int opcion;
        // esto lo estoy haciendo para poder hacer el commit
        System.out.println("Menu de conversion");
        System.out.println("1.-Cambio de unidades de horas a segundos");
        System.out.println("2.-Cambio de unidades de kilometros a metros");
        System.out.println("3.-Cambio de kilometros horas a metros segundo");
        System.out.println("4.-Salir");
        System.out.println("Escribe una de las opciones: ");
        opcion=sc.nextInt();
        
        switch(opcion){
            case 1:
                System.out.println("Has seleccionado la opcion de cambiar de horas a segundos");
                System.out.println("Introduce las horas");
                int horas=sc.nextInt();
                int resultado=horas*3600;
                System.out.println("Las horas introducidas equivalen a: " + resultado + " segundos");
            break;
            
            case 2:
                System.out.println("Has seleccionado la opcion de cambiar de kilometros a metros");
                System.out.println("Introduce los kilometros");
                int kilometros=sc.nextInt();
                int resultado1=kilometros*1000;
                System.out.println("Los kilometros introducidos equivalen a: " + resultado1 + " metros");
            break;
            
            case 3:
                System.out.println("Has seleccionado la opcion de cambiar de kilometros horas a metros segundo");
                System.out.println("Introduce los kilometros horas");
                int kilometros_horas=sc.nextInt();
                double resultado2=kilometros_horas/3.6;
                System.out.println("Los kilometros horas introducidos equivalen a: " + resultado2 + " metros segundos");
            
            case 4:
                salir=true;
                System.out.println("Has escogido la opcion de salir del menu");
            break;
            
            default:
                System.out.println("Solo numeros entre el 1 y el 4");
        }
    }
    
}
